from flake8_lending import __version__

MAC = "mac"
UNIX = "unix"
DOS = "dos"

LINE_ENDINGS = {
    UNIX: "\n",
    DOS: "\r\n",
    MAC: "\r",
}

DEFAULT_LINE_ENDING = UNIX

L101 = "L101 unexpected line-ending: expected {}, found {}"


def check_line_ending(physical_line):
    """
    The actual checker logic. Called on each physical line on each scanned file

    :param physical_line: The line currently being scanned.
    :return:
    """
    style = infer_ending_style(physical_line)
    if style != check_line_ending.expected_style:
        msg = L101.format(check_line_ending.expected_style, style)
        return len(physical_line) - 1, msg


def add_options(option_manager):
    option_manager.add_option(
        '--line-ending', type='choice', choices=list(LINE_ENDINGS.keys()),
        default=DEFAULT_LINE_ENDING, parse_from_config=True,
        dest="line_ending",
        help="The expected line-ending style in all files. "
             "(Default is '%default', and this code will REALLY appreciate if "
             "you'll keep it)",
    )


def parse_options(options):
    check_line_ending.expected_style = options.line_ending


check_line_ending.name = "flake8-lending"
check_line_ending.version = __version__
check_line_ending.expected_style = DEFAULT_LINE_ENDING
check_line_ending.add_options = add_options
check_line_ending.parse_options = parse_options


def infer_ending_style(line):
    """
    Infer the ending style of a line.

    :param line: The line, possibly with a line-separator.
    :return: The style detected, or `None` if the line does not contain any
        line-separator
    """
    # Testing for DOS first because line that ends with \r\n obviously ends
    # with \n, yet it is not in UNIX style
    if line.endswith(LINE_ENDINGS[DOS]):
        style = DOS
    elif line.endswith(LINE_ENDINGS[UNIX]):
        style = UNIX
    elif line.endswith(LINE_ENDINGS[MAC]):
        style = MAC
    else:
        style = None  # Line does not have a separator at the end
    return style
